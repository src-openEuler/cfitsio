Name:              cfitsio
Version:           4.6.0
Release:           1
Summary:           Library for manipulating FITS data files
License:           MIT
URL:               https://heasarc.gsfc.nasa.gov/fitsio/
Source0:           https://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio-%{version}.tar.gz

Patch0001:         cfitsio-noversioncheck.patch
Patch0004:         add-support-for-loongarch64.patch

BuildRequires:     gcc-gfortran zlib-devel bzip2-devel chrpath curl-devel make

%description
CFITSIO is a library of C and FORTRAN subroutines for reading and writing data files
in FITS (Flexible Image Transmission System) data format. CFITSIO frees programmers
from the internal complexity of the FITS file format by providing a set of easy-to-use
high-level routines, thereby simplifying the task of writing software that processes
FITS files. At the same time, CFITSIO provides many advanced features, making it the
most widely used FITS file programming interface in the astronomy community.

%package devel
Summary:            Required headers when building programs against cfitsio.
Requires:           %{name} = %{version}-%{release}
Requires:           curl-devel zlib-devel bzip2-devel
Provides:           %{name}-static = %{version}-%{release}
Obsoletes:          %{name}-static < %{version}-%{release}

%description devel
Header files needed when building a program against the cfitsio library.
Static cfitsio library.

%package help
Summary:            Documentation for cfitsio
BuildArch:          noarch
Provides:           %{name}-docs = %{version}-%{release}
Obsoletes:          %{name}-docs < %{version}-%{release}

%description help
Stand-alone documentation for cfitsio.

%package utils
Summary: FITS image compression and decompression utilities
Requires: %{name} = %{version}-%{release}
Provides: fpack{?_isa} = %{version}-%{release}
Obsoletes: fpack < 4.5.0
Provides: fitsverify{?_isa} = 4.22-5
Obsoletes: fitsverify < 4.22-4

%description utils
This package contains utility programas provided by CFITSIO

%prep
%autosetup  -n %{name}-%{version} -p1

%build
%configure --enable-reentrant --with-bzip2 --includedir=%{_includedir}/%{name}
%make_build

%check
make testprog
LD_LIBRARY_PATH=. ./testprog > testprog.lis
cmp -s testprog.lis testprog.out
cmp -s testprog.fit testprog.std

%install
%make_install

rm %{buildroot}/%{_bindir}/cookbook
rm %{buildroot}/%{_bindir}/smem
rm %{buildroot}/%{_bindir}/speed

chrpath -d %{buildroot}%{_bindir}/f{,un}pack

%files
%doc README.md ChangeLog
%license licenses/License.txt
%{_libdir}/libcfitsio.so.*

%files devel
%doc utilities/cookbook.*
%{_includedir}/%{name}
%{_libdir}/libcfitsio.so
%{_libdir}/pkgconfig/cfitsio.pc
%{_libdir}/libcfitsio.a

%files help
%doc docs/fitsio.pdf docs/cfitsio.pdf

%files utils
%doc docs/fpackguide.pdf
%license licenses/License.txt
%{_bindir}/fitsverify
%{_bindir}/fitscopy
%{_bindir}/fpack
%{_bindir}/funpack
%{_bindir}/imcopy

%changelog
* Sat Mar 15 2025 Funda Wang <fundawang@yeah.net> - 4.6.0-1
- update to 4.6.0
- add requires on upstream devel packages, as they are referenced
  in Libs.private of pkgconfig file

* Mon Oct 28 2024 Funda Wang <fundawang@yeah.net> - 4.5.0-1
- update to 4.5.0

* Sun Jun 9 2024 zhangxianjun <zhangxianjun@kylinos.cn> - 4.3.0-2
- add support for loongarch64

* Thu Sep 14 2023 liyanan <thistleslyn@163.com> - 4.3.0-1
- update to 4.3.0

* Wed Aug 24 2022 caodongxia <caodongxia@h-partners.com> -3.490-2
- Remove rpath

* Wed Feb 09 2022 yaoxin <yaoxin30@huawei.com> - 3.490-1
- Upgrade cfitsio to 3.490 to fix CVE-2018-3848,CVE-2018-3849

* Mon Mar 09 2020 yangjian<yangjian79@huawei.com> - 3.450-5
- Fix changelog  problem

* Mon Mar 09 2020 yangjian<yangjian79@huawei.com> - 3.450-4
- To fix files problem

* Wed Mar 04 2020 yangjian<yangjian79@huawei.com> - 3.450-3
- Package init
